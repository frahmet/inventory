#!/bin/bash

cd $PRJ_SRC
python manage.py migrate
python manage.py createsuperuser

#db migrations
/usr/local/bin/gunicorn inventory.wsgi:application \
    --workers 4 \
    --bind :$APPLICATION_PORT \
    --log-level=info
