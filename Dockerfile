FROM python:3.6

# Set env variables used in this Dockerfile
ENV PYTHONUNBUFFERED 1

ENV PRJ_SRC=/src

WORKDIR $PRJ_SRC
COPY requirements.txt $PRJ_SRC/requirements.txt
RUN pip install -r $PRJ_SRC/requirements.txt

COPY . $PRJ_SRC

COPY ./docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
