from django.urls import path

from house.views import (FacilityList, FacilityDetail, RoomList, RoomDetail,
                         HouseList, HouseDetail, TotalPrice)

urlpatterns = [
    path('houses/<str:house_code>/rooms/<str:room_code>/facilities',
         FacilityList.as_view()),
    path('houses/<str:house_code>/rooms/<str:room_code>/facilities/<str:facility_code>', # noqa
         FacilityDetail.as_view()),
    path('houses/<str:house_code>/rooms',
         RoomList.as_view()),
    path('houses/<str:house_code>/rooms/<str:room_code>',
         RoomDetail.as_view()),
    path('houses', HouseList.as_view()),
    path('houses/<str:code>', HouseDetail.as_view()),
    path('total_price', TotalPrice.as_view()),
]
