from rest_framework import generics, views, status
from rest_framework.response import Response

from django.shortcuts import get_object_or_404

from house.serializers import (FacilitySerializer, RoomSerializer,
                               HouseSerializer, TotalPriceSerializer)
from house.models import Facility, House, Room


class FacilityList(generics.ListCreateAPIView):
    queryset = Facility.objects.all()
    serializer_class = FacilitySerializer

    def get_queryset(self):
        house_code = self.kwargs.get('house_code')
        room_code = self.kwargs.get('room_code')
        house = get_object_or_404(House, code=house_code)
        room = get_object_or_404(Room, code=room_code)
        return Facility.objects.filter(room__house=house,
                                       room=room)

    def perform_create(self, serializer):
        facility = serializer.save()
        room_code = self.kwargs.get('room_code')
        room = get_object_or_404(Room, code=room_code)
        room.facilities.add(facility)


class FacilityDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Facility.objects.all()
    serializer_class = FacilitySerializer

    def get_object(self):
        house_code = self.kwargs.get('house_code')
        room_code = self.kwargs.get('room_code')
        facility_code = self.kwargs.get('facility_code')
        house = get_object_or_404(House, code=house_code)
        room = get_object_or_404(Room, code=room_code)
        return get_object_or_404(Facility, room__house=house, room=room,
                                 code=facility_code)


class RoomList(generics.ListCreateAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    def get_queryset(self):
        house_code = self.kwargs.get('house_code')
        house = get_object_or_404(House, code=house_code)
        return Room.objects.filter(house=house)

    def perform_create(self, serializer):
        room = serializer.save()
        house_code = self.kwargs.get('house_code')
        house = get_object_or_404(House, code=house_code)
        house.rooms.add(room)


class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer

    def get_object(self):
        house_code = self.kwargs.get('house_code')
        room_code = self.kwargs.get('room_code')
        house = get_object_or_404(House, code=house_code)
        return get_object_or_404(Room, house=house, code=room_code)


class HouseList(generics.ListCreateAPIView):
    queryset = House.objects.all()
    serializer_class = HouseSerializer


class HouseDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = House.objects.all()
    serializer_class = HouseSerializer

    def get_object(self):
        house_code = self.kwargs.get('code')
        return get_object_or_404(House, code=house_code)


class TotalPrice(views.APIView):
    serializer_class = TotalPriceSerializer

    def post(self, request, *args, **kwargs):
        ser = self.serializer_class(data=request.data)
        ser.is_valid(raise_exception=True)
        price = ser.save()
        return Response({"total_price": price}, status=status.HTTP_200_OK)
