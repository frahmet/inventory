from rest_framework import serializers

from django.shortcuts import get_object_or_404
from django.db.models import Sum

from house.models import Facility, House, Room


class FacilitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Facility
        fields = ('id', 'code', 'name', 'price')


class RoomSerializer(serializers.ModelSerializer):
    facilities = FacilitySerializer(many=True, read_only=True)

    class Meta:
        model = Room
        fields = ('id', 'code', 'name', 'facilities')

    def create(self, validated_data):
        room = Room.objects.create(**validated_data)
        if "facilities" in self.initial_data:
            facilities = self.initial_data.pop('facilities')
            for facility in facilities:
                facility_obj, _ = Facility.objects.get_or_create(**facility)
                room.facilities.add(facility_obj)
            room.save()
        return room

    def update(self, instance, validated_data):
        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.save()
        if "facilities" in self.initial_data:
            facilities = self.initial_data.pop('facilities')
            instance.facilities.clear()
            for facility in facilities:
                facility_obj, _ = Facility.objects.get_or_create(**facility)
                instance.facilities.add(facility_obj)
            instance.save()
        return instance


class HouseSerializer(serializers.ModelSerializer):
    rooms = RoomSerializer(many=True, read_only=True)

    class Meta:
        model = House
        fields = ('id', 'code', 'name', 'rooms')

    def create(self, validated_data):
        house = House.objects.create(**validated_data)
        if "rooms" in self.initial_data:
            rooms = self.initial_data.pop('rooms')
            for room in rooms:
                room_obj, _ = Room.objects.get_or_create(**room)
                house.rooms.add(room)
            house.save()
        return house

    def update(self, instance, validated_data):
        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.save()
        if "rooms" in self.initial_data:
            rooms = self.initial_data.pop('rooms')
            instance.rooms.clear()
            for room in rooms:
                room_obj, _ = Room.objects.get_or_create(**room)
                instance.rooms.add(room_obj)
            instance.save()
        return instance


class TotalPriceSerializer(serializers.Serializer):
    house_code = serializers.CharField(required=False)
    room_code = serializers.CharField(required=False)

    def validate_house_code(self, house_code):
        if not house_code:
            return
        return get_object_or_404(House, code=house_code)

    def validate_room_code(self, room_code):
        if not room_code:
            return
        house = self.initial_data.get('house_code')
        if not house:
            return
        return get_object_or_404(Room, code=room_code, house__code=house)

    def save(self):
        data = self.validated_data
        room = data.get('room_code')
        house = data.get('house_code')
        filter_qs = {}
        if room:
            filter_qs['room'] = room
        if house:
            filter_qs['room__house'] = house

        total = Facility.objects.filter(
            **filter_qs).aggregate(total=Sum('price'))

        return total.get('total')
