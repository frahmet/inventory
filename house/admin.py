from django.contrib import admin

from house import models

admin.site.register(models.Facility)
admin.site.register(models.Room)
admin.site.register(models.House)
