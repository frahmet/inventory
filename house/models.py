from django.db import models


class Facility(models.Model):
    code = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    price = models.DecimalField(decimal_places=2, max_digits=8)

    def __str__(self):
        return "[%s]-%s (%.2f)" % (self.code, self.name, self.price)


class Room(models.Model):
    code = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    facilities = models.ManyToManyField(Facility, null=True, blank=True)

    def __str__(self):
        return "[%s]-%s" % (self.code, self.name)


class House(models.Model):
    code = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    rooms = models.ManyToManyField(Room)

    def __str__(self):
        return "[%s]-%s" % (self.code, self.name)
